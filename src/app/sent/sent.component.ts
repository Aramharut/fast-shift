import {Component, OnInit} from '@angular/core';
import {messages} from "../general/Types/messages";
import {Observable} from "rxjs";
import {MessagesService} from "../services/messages.service";
import {GeneralService} from "../general/general.service";
import {map, tap} from "rxjs/operators";

@Component({
  selector: 'app-sent',
  templateUrl: './sent.component.html',
  styleUrls: ['./sent.component.scss']
})
export class SentComponent implements OnInit {

  set searchValue(value: string) {
    this.searchData(value);
    this._searchValue = value;
  }

  allMessages: messages.list[];
  private _searchValue: string;
  readonly TABLE_HEADERS = ['#', 'Messages'];
  messagesList$: Observable<messages.list[]>;
  jwt = this.generalService.getLocalStorage('jwt');

  constructor(public messageService: MessagesService,
              public generalService: GeneralService) {
  }

  ngOnInit(): void {
    this.messagesList$ = this.messageService.messagesList().pipe(
      map(messages => messages.data.filter(msg => {
        return msg.email === this.jwt
      })),
      tap(data => {
        this.allMessages = data;
      }));
    this.messagesList$.subscribe();
  }

  searchData(searchValue: string): void {
    this.messagesList$.pipe(
      map(data => {
          searchValue = searchValue.trim();
          if (searchValue === "") {
            return data;
          }
          return data.filter(messages => {
              return messages.text.toLowerCase().includes(searchValue.toLowerCase());
            }
          )
        }
      ),
      tap(data => {
        this.allMessages = data;
      })
    ).subscribe();
  }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {SentRoutingModule} from "./sent-routing.module";
import {SentComponent} from "./sent.component";
import {HeaderModule} from "../header/header.module";

@NgModule({
  declarations: [SentComponent],
  imports: [
    SentRoutingModule,
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    HeaderModule
  ]
})
export class SentModule {
}

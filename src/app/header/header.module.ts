import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {HeaderRoutingModule} from "./header-routing.module";
import {HeaderComponent} from "./header.component";
import {MaterialModule} from "../general/Modules/material.module";

@NgModule({
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    HeaderRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ]
})
export class HeaderModule {
}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {GeneralService} from "../general/general.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() searchName = new EventEmitter<string>();
  searchControl: FormControl = new FormControl('');
  searchForm: FormGroup = new FormGroup({
    search: this.searchControl,
  });

  constructor(
    public generalService: GeneralService,
    public router: Router) {
  }

  ngOnInit(): void {
  }

  logOut(): void {
    this.generalService.clearLocalStorage();
    this.router.navigateByUrl('/login');
  }

  searchBtn(): void {
    this.searchName.emit(this.searchForm.value.search);
  }
}

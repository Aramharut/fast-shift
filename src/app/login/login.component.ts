import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RegistrationErrorStateMatcher} from "../general/Validations/registration";
import {UsersService} from "../services/users.service";
import {GeneralService} from "../general/general.service";
import {tap} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  emailControl: FormControl = new FormControl('', [Validators.required, Validators.email,]);
  passwordControl: FormControl = new FormControl('', [Validators.required,]);
  loginForm: FormGroup = new FormGroup({
    email: this.emailControl,
    password: this.passwordControl
  });
  matcher: RegistrationErrorStateMatcher = new RegistrationErrorStateMatcher();
  beckErr: boolean = false;

  constructor(
    public usersService: UsersService,
    public generalService: GeneralService,
    public router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm.status === "VALID") {
      this.beckErr = false;
      this.usersService.login(this.loginForm.value).pipe(tap(data => {
        if (data && data.email) {
          this.generalService.setLocalStorage('jwt', data.email);
          this.router.navigateByUrl('/dashboard');
        } else {
          this.beckErr = true;
        }
      }))
        .subscribe();
    }
  }
}

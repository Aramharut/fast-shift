import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {GeneralService} from "./general.service";

@Injectable()
export class CanActivateTeam implements CanActivate {
  constructor(public generalService: GeneralService,) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.generalService.getLocalStorage('jwt')) {
      return true;
    }
    return false;
  }
}

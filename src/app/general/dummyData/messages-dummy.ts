import {Observable, of} from "rxjs";
import {messages} from "../Types/messages";

export class MessagesDummy {
  constructor() {}
  messages: messages.list[] = [
    {
      email: 't@t.tt',
      text: 'Lorem ipsum'
    },
    {
      email: 'test@test.test',
      text: 'bla bla'
    },
    {
      email: 't@t.tt',
      text: 'Lorem ipsum'
    },
    {
      email: '11test@test.test',
      text: 'bla bla'
    },
    {
      email: 't@t.tt',
      text: 'Lorem ipsum'
    },
    {
      email: 'test@test.test',
      text: 'dxwesf rtgfrsede edrszf htrbdfxc rtgdfcx bla'
    },
  ];

  getAllMessages():Observable<messages.ResponseList>{
    return of({
      status: 'success',
      data: this.messages
    });
  }

  sendMessages(data: messages.list[]):Observable<{ data: messages.list[]; status: string }>{
    this.messages = [...this.messages, ...data];
    return of({
      status: 'success',
      data
    });
  }
}

import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor() {
  }

  setLocalStorage(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  getLocalStorage(key: string): string {
    return localStorage.getItem(key);
  }

  clearLocalStorage() {
    localStorage.clear();
  }
}

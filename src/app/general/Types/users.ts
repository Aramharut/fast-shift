export namespace users {
  export class user {
    firstName: string;
    lastName: string;
    email: string;
    gender: string;
    password: string;
  }

  export class login {
    email: string;
    password: string;
  }
}

export namespace messages {
  export class list {
    email: string;
    text: string;
  }
  export class ResponseList {
    status: string;
    data: messages.list[]
  }
}

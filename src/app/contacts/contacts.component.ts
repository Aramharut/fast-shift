import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {UsersService} from "../services/users.service";
import {Observable} from "rxjs";
import {map, tap} from "rxjs/operators";
import {users} from "../general/Types/users";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit, OnChanges {
  usersList$: Observable<users.user[]>;
  usersList: users.user[];
  @Input() searchName: string;

  constructor(public usersService: UsersService) {
  }

  ngOnInit(): void {
    this.usersList$ = this.usersService.usersList().pipe(tap(data => this.usersList = data));
    this.usersList$.subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.searchName.firstChange) {
      this.usersList$.pipe(
        map(data => {
            if (this.searchName.trim() === "") {
              return data;
            }
            return data.filter(users => {
                return users.email.toLowerCase().includes(this.searchName.toLowerCase()) ||
                  users.firstName.toLowerCase().includes(this.searchName.toLowerCase()) ||
                  users.lastName.toLowerCase().includes(this.searchName.toLowerCase()) ||
                  users.gender.toLowerCase().includes(this.searchName.toLowerCase());
              }
            )
          }
        ),
        tap(data => {
          this.usersList = data;
        })
      ).subscribe();
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContactsRoutingModule} from './contacts-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import {MessagesModule} from "../messages/messages.module";
import {ContactsComponent} from "./contacts.component";

@NgModule({
    declarations: [
        ContactsComponent,
    ],
  imports: [
    CommonModule,
    ContactsRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
    MessagesModule,
  ],
  exports: [
    MatButtonModule,
    ContactsComponent
  ]
})
export class ContactsModule { }

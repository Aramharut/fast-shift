import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import {DashboardComponent} from "./dashboard.component";
import {MessagesModule} from "../messages/messages.module";
import {ContactsModule} from "../contacts/contacts.module";
import {HeaderModule} from "../header/header.module";

@NgModule({
    declarations: [
        DashboardComponent,
    ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
    MessagesModule,
    ContactsModule,
    HeaderModule,
  ],
  exports: [
    MatButtonModule
  ]
})
export class DashboardModule { }

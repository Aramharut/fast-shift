import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "./general/Modules/material.module";
import {UsersDummy} from "./general/dummyData/users-dummy";
import {MessagesDummy} from "./general/dummyData/messages-dummy";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    // ReactiveFormsModule,
  ],
  providers: [
    UsersDummy,
    MessagesDummy
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RegistrationErrorStateMatcher} from "../general/Validations/registration";
import {UsersService} from "../services/users.service";
import {Router} from "@angular/router";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  readonly genders: string[] = ['Male', 'Female'];
  emailControl: FormControl = new FormControl('', [Validators.required, Validators.email,]);
  firstNameControl: FormControl = new FormControl('', [Validators.required]);
  lastNameControl: FormControl = new FormControl('', [Validators.required]);
  genderControl: FormControl = new FormControl('', [Validators.required]);
  passwordControl: FormControl = new FormControl('', [Validators.required]);
  registerForm: FormGroup = new FormGroup({
    firstName: this.firstNameControl,
    lastName: this.lastNameControl,
    email: this.emailControl,
    gender: this.genderControl,
    password: this.passwordControl
  });

  matcher: RegistrationErrorStateMatcher = new RegistrationErrorStateMatcher();

  constructor(public usersService: UsersService,
              public router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  registration(): void {
    if (this.registerForm.status === "VALID") {
      this.usersService.addUser(this.registerForm.value)
        .pipe(
          tap(() => this.router.navigateByUrl('/login'))
        )
        .subscribe();
    }
  }
}

import {Inject, Injectable} from '@angular/core';

import {UsersDummy} from "../general/dummyData/users-dummy";
import {users} from "../general/Types/users";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UsersService {


  constructor(@Inject(UsersDummy) public usersDummy) {

  }

  usersList(): Observable<users.user[]> {
    return this.usersDummy.getUsersList();
  }

  addUser(user: users.user): Observable<users.user> {
    return this.usersDummy.putUser(user);
  }

  login(data: users.login): Observable<users.user> {
    return this.usersDummy.getUser(data);
  }
}

import {Inject, Injectable} from '@angular/core';
import {users} from "../general/Types/users";
import {Observable} from "rxjs";
import {MessagesDummy} from "../general/dummyData/messages-dummy";
import {messages} from "../general/Types/messages";

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(@Inject(MessagesDummy) public messagesDummy) {
  }

  messagesList(): Observable<messages.ResponseList> {
    return this.messagesDummy.getAllMessages();
  }

  sendMessage(data: messages.list[]): Observable<{ status: string; data: users.user[] }> {
    return this.messagesDummy.sendMessages(data);
  }
}

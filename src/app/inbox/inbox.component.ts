import {Component, OnInit} from '@angular/core';
import {MessagesService} from "../services/messages.service";
import {Observable} from "rxjs";
import {map, tap} from "rxjs/operators";
import {GeneralService} from "../general/general.service";
import {messages} from "../general/Types/messages";

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  set searchValue(value: string) {
    this.searchData(value);
    this._searchValue = value;
  }

  private _searchValue: string;
  messagesList$: Observable<messages.list[]>;
  jwt = this.generalService.getLocalStorage('jwt');
  readonly TABLE_HEADERS = ['#', 'Owner', 'Messages'];
  allMessages: messages.list[];

  constructor(public messageService: MessagesService,
              public generalService: GeneralService) {
  }

  ngOnInit(): void {
    this.messagesList$ = this.messageService.messagesList().pipe(
      map(messages => messages.data.filter(msg => {
        return msg.email !== this.jwt
      })),tap(data => {
        this.allMessages = data;
      }));
    this.messagesList$.subscribe();
  }

  searchData(searchData: string) {
    this.messagesList$.pipe(
      map(data => {
        searchData = searchData.trim();
          if (searchData === "") {
            return data;
          }
          return data.filter(messages => {
              return messages.text.toLowerCase().includes(searchData.toLowerCase()) ||
                messages.email.toLowerCase().includes(searchData.toLowerCase());
            }
          )
        }
      ),
      tap(data => {
        this.allMessages = data;
      })
    ).subscribe();
  }
}

import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InboxComponent} from "./inbox.component";

const routes: Routes = [
  {
    path: '',
    component: InboxComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  providers: [],
})
export class InboxRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {InboxRoutingModule} from "./inbox-routing.module";
import {InboxComponent} from "./inbox.component";
import {HeaderModule} from "../header/header.module";

@NgModule({
  declarations: [InboxComponent],
  imports: [
    InboxRoutingModule,
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    HeaderModule
  ]
})
export class InboxModule { }

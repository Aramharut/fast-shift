import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {messages} from "../general/Types/messages";
import {map, tap} from "rxjs/operators";
import {GeneralService} from "../general/general.service";
import {FormControl, FormGroup} from "@angular/forms";
import {MessagesService} from "../services/messages.service";
import {users} from "../general/Types/users";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit, OnChanges {
  @Input() searchValue: string;
  message: FormControl = new FormControl('', []);
  messageForm: FormGroup = new FormGroup({
    message: this.message,
  });

  jwt: string = this.generalService.getLocalStorage('jwt');
  public messages$: Observable<any[]>;
  private newMessages$: Observable<{ status: string; data: users.user[] }>;
  public allMessages: messages.list[] = [];
  modelMessage: string;

  constructor(public generalService: GeneralService,
              public messagesService: MessagesService) {
  }

  ngOnInit(): void {
    this.messagesList();
  }

  messagesList(): void {
    this.messages$ = this.messagesService.messagesList().pipe(map(messages => messages.data));
    this.messages$.subscribe(messages => {
      this.allMessages = messages;
    })
  }

  sendMsg(): void {
    const messages = [{
      email: this.jwt,
      text: this.messageForm.value.message
    }];

    this.messagesService.sendMessage(messages).subscribe(() => {
      this.messages$ = this.messages$.pipe(map(data => {
        this.modelMessage = '';
        return [...data, ...messages];
      }));
    });

    this.messages$.subscribe(messages => {
      if (this.searchValue && this.searchValue.trim().length) {
        this.searchMessage();
      } else {
        this.allMessages = messages.reverse();
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.searchValue.firstChange) {
      this.searchMessage();
    }
  }

  searchMessage(): void {
    this.messages$.pipe(
      map(data => {
          this.searchValue = this.searchValue.trim();
          if (this.searchValue === "") {
            return data;
          }
          return data.filter(messages => {
              return messages.text.toLowerCase().includes(this.searchValue.toLowerCase());
            }
          )
        }
      ),
      tap(data => {
        this.allMessages = data;
      })
    ).subscribe();
  }

}

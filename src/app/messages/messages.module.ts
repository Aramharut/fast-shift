import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MessagesRoutingModule} from './messages-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import {MessagesComponent} from "./messages.component";

@NgModule({
  declarations: [
    MessagesComponent,
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
  ],
  exports: [
    MatButtonModule,
    MessagesComponent
  ]
})
export class MessagesModule {
}
